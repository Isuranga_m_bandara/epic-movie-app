import 'dart:async';

import 'package:flutter/material.dart';
import 'package:movie/Intro_Page/onboard.dart';


class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);
  static const routeName = "/splash";

  void main() {
    runApp(SplashScreen());
  }

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState(){
    super.initState();
    Timer(Duration(seconds: 3),()=>Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (BuildContext context) => IntroSliderPage1 ())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(decoration: BoxDecoration(
            image: DecorationImage(image:
            AssetImage("assets/images/bg.png"),
                fit: BoxFit.cover),

          ),
          ),

              Container(

                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image(image: AssetImage("assets/images/movie_icon.png"),
                          height: 100.0,
                          width: 100.0,),

                      ],
                    ),
                  ),

              Container(

                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children:<Widget> [

                      Text(
                          "Movie App",
                          textAlign: TextAlign.end,
                          style: TextStyle(
                          color: Colors.white,
                          fontFamily: "French Script MT Regular",
                          fontSize: 28.0,
                          ),
                      ),
                    ],
                  ),
              ),
            ],

          ),

      
    );
  }
}



