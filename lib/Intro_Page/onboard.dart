import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';
import 'package:movie/features/feature_login/presentation/pages/login.dart';
import 'package:movie/widgets/b.dart';


class IntroSliderPage1 extends StatefulWidget {
  const IntroSliderPage1({Key? key}) : super(key: key);

  @override
  _IntroSliderPageState createState() => _IntroSliderPageState();
}

class _IntroSliderPageState extends State<IntroSliderPage1> {


  List<Slide> slides = [];

  @override
  void initState() {SizedBox(height: 100);
    // TODO: implement initState
    super.initState();

    slides.add(new Slide(
      backgroundImage: "assets/images/bg.png",

      title: "Lorem lpsum",
      styleTitle: TextStyle(
        color: Colors.white,
        fontFamily: "Open Sans ExtraBold Regular",
        fontSize: 20.0,
      ),
      description: "Lorem lpsum is simply dummy text of the printing and typesetting industry. Lorem lpsum has been the industry's standard dummy text ever since the 1500s",
      styleDescription: TextStyle(
        color: Colors.white,
        fontFamily: "Open Sans Light Regular",
        fontSize: 15.0,
      ),
      pathImage: "assets/images/intro/graphic_1.png",

    ));

    slides.add(new Slide(
      title: "Why do we use it?",
      styleTitle: TextStyle(
        color: Colors.white,
        fontFamily: "Open Sans ExtraBold Regular",
        fontSize: 20.0,
      ),
      description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem lpsum is that it. ",
      styleDescription: TextStyle(
        color: Colors.white,
        fontFamily: "Open Sans Light Regular",
        fontSize: 15.0,
      ),
      pathImage: "assets/images/intro/graphic_2.png",
    ));

    slides.add(new Slide(
      title: "Where does it come from?",
      styleTitle: TextStyle(
        color: Colors.white,
        fontFamily: "Open Sans ExtraBold Regular",
        fontSize: 20.0,
      ),
      description: "Contrary to popular belief, Lorem lpsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.",
      styleDescription: TextStyle(
        color: Colors.white,
        fontFamily: "Open Sans Light Regular",
        fontSize: 15.0,
      ),
      pathImage: "assets/images/intro/graphic_3.png",


    ));
  }


  List<Widget> renderListCustomTabs() {
    List<Widget> tabs = [];
    for (int i = 0; i < slides.length; i++) {
      Slide currentSlide = slides[i];
      tabs.add(Container(
        width: double.infinity,
        height: double.infinity,

        child: BackgroundHome1(

          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(

            children: [
              Container(
                  margin: EdgeInsets.only(
                      top: 20, left: 10, right: 10, bottom: 10),
                  // decoration: BoxDecoration(
                  //   color: const Color(0xFF0000000).withOpacity(0.06),
                  //   border: new Border.all(color: Colors.transparent),
                  //   borderRadius: new BorderRadius.only(
                  //       topLeft: const Radius.circular(20.0),
                  //       topRight: const Radius.circular(20.0),
                  //       bottomLeft: const Radius.circular(20.0),
                  //       bottomRight: const Radius.circular(20.0)
                  //   ),
                  // ),
                  child: SizedBox(height: 250,width: 200,
                    child: Image.asset(
                      currentSlide.pathImage.toString(),
                      matchTextDirection: true,

                      fit: BoxFit.contain,
                    ),
                  )),
              Container(
                child: Text(
                  currentSlide.title.toString(),
                  style: currentSlide.styleTitle,
                  textAlign: TextAlign.center,

                ),

              ),
              Container(
                padding: EdgeInsets.fromLTRB(20, 10, 20, 20),
                child: Text(
                  currentSlide.description.toString(),
                  style: currentSlide.styleDescription,
                  textAlign: TextAlign.center,
                  maxLines: 5,

                ),

              ),
              Container(


              ),


            ],
          ),
        ),
      ));
    }
    return tabs;
  }

  @override
  Widget build(BuildContext context) {
    return IntroSlider(

      backgroundColorAllSlides: Colors.pinkAccent,

      renderNextBtn: Text("Next", style: TextStyle(color: Colors.white,

           ),),
      renderPrevBtn: Text("Previous", style: TextStyle(color: Colors.white),),
      renderDoneBtn: Text("Done", style: TextStyle(color: Colors.white),),
      colorDot: Colors.black26,
      colorActiveDot: Colors.white,



      sizeDot: 8.0,


      listCustomTabs: this.renderListCustomTabs(),
      scrollPhysics: BouncingScrollPhysics(),
      hideStatusBar: true,
      onDonePress: () =>
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (_) => LoginPage(),
              )),
    );
  }
}
