import 'package:flutter/material.dart';

import 'package:movie/core/injection/injection_container.dart' as ij;
import 'package:movie/splash_screen.dart';

void main() async {
  await ij.init();
  runApp(MyApp());
  //RemoteDataSourceImpl _remote = RemoteDataSourceImpl();
  //Future<GetLoginResponse> loginReq = _remote.loginRequest(GetLoginRequest(
  //reqCode:101, username: "arvinjayanake", password: "YWJjMTIz#e"));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: const SplashScreen(),
      theme: ThemeData(
        errorColor: Colors.black,
        // "fontFamily: "Open Sans ExtraBold Regular""
      ),
    );
  }
}
