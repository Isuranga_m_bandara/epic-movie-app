class Movies {
  final String id;
  final String title;
  final String image;
  final String hash;
  final String description;
  final String rating;
  final String year;


  Movies({
    required this.id,
    required this.title,
    required this.image,
    required this.hash,
    required this.description,
    required this.rating,
    required this.year,

  });
}

final movieList = [
  Movies(
    id: '1',
      title: 'Spider Man',
      image:
      'assets/images/home/spider_man_1.jpg',
      hash: 'Action | Adventure | SiFi',
      description:
      ' Spider-Man is a superhero created by writer-editor Stan Lee and writer-artist Steve Ditko. He first appeared in the anthology comic book Amazing Fantasy #15 (Aug. 1962) in the Silver Age of Comic Books. He appears in American comic books published by Marvel Comics, as well as in a number of movies, television shows, and video game adaptations set in the Marvel Universe.',
      rating: '8.2',
      year: '2002',
      ),
  Movies(
    id: '2',
      title: 'The Matrix',
      image:
      'assets/images/home/matrix.jpg',
      hash: 'Action | SiFi',
      description:
      'The Matrix (1999) and continuing with three sequels, The Matrix Reloaded and The Matrix Revolutions (both in 2003) and The Matrix Resurrections (2021), with the first three films written and directed by the Wachowskis and produced by Joel Silver, while the fourth film is directed by Lana Wachowski and written by Wachowski, David Mitchell and Aleksander Hemon.',
      rating: '8.7',
      year: '1999',
      ),
  Movies(
    id: '3',
      title: 'The Thing',
      image:
      'assets/images/home/thing.jpg',
      hash: 'Action | Mystory | SiFi',
      description:
      'The Thing: Directed by Matthijs van Heijningen Jr.. With Mary Elizabeth Winstead, Joel Edgerton, Ulrich Thomsen, Eric Christian Olsen. At an Antarctica research site, the discovery of an alien craft leads to a confrontation between graduate student Kate Lloyd and scientist Dr. Sander Halvorson.',
      rating: '6.2',
      year: '2011',
      ),
  Movies(
    id: '4',
      title: 'Predator',
      image:
      'assets/images/home/predator.jpg',
      hash: 'Action | Adventure | SiFi',
      description:
      'Predator: Directed by John McTiernan. With Arnold Schwarzenegger, Carl Weathers, Elpidia Carrillo, Bill Duke. A team of commandos on a mission in a Central American jungle find themselves hunted by an extraterrestrial warrior..',
      rating: '7.8',
      year: '1987',
      ),

];
