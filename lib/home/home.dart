// import 'package:flutter/cupertino.dart';
//
// import 'package:flutter/material.dart';
//
// import 'package:movie/widgets/background_home.dart';
//
//
// class ListViewPage extends StatefulWidget {
//
//   @override
//
//   _ListViewPageState createState() => _ListViewPageState();
//
// }
//
//
// class _ListViewPageState extends State<ListViewPage> {
//
//   // Title List Here
//
//   var titleList = [
//
//     "Spider Man",
//
//     "The Matrix",
//
//     "The Thing",
//
//     "Predator"
//
//
//   ];
//
//
//   // Description List Here
//
//   var descList = [
//
//     "Spider-Man is a superhero created by writer-editor Stan Lee and writer-artist Steve Ditko. He first appeared in the anthology comic book Amazing Fantasy #15 (Aug. 1962) in the Silver Age of Comic Books. He appears in American comic books published by Marvel Comics, as well as in a number of movies, television shows, and video game adaptations set in the Marvel Universe.",
//
//     " The Matrix (1999) and continuing with three sequels, The Matrix Reloaded and The Matrix Revolutions (both in 2003) and The Matrix Resurrections (2021), with the first three films written and directed by the Wachowskis and produced by Joel Silver, while the fourth film is directed by Lana Wachowski and written by Wachowski, David Mitchell and Aleksander Hemon.",
//
//     " The Thing: Directed by Matthijs van Heijningen Jr.. With Mary Elizabeth Winstead, Joel Edgerton, Ulrich Thomsen, Eric Christian Olsen. At an Antarctica research site, the discovery of an alien craft leads to a confrontation between graduate student Kate Lloyd and scientist Dr. Sander Halvorson.",
//
//     " Predator: Directed by John McTiernan. With Arnold Schwarzenegger, Carl Weathers, Elpidia Carrillo, Bill Duke. A team of commandos on a mission in a Central American jungle find themselves hunted by an extraterrestrial warrior..",
//
//
//   ];
//
//
//   // Image Name List Here
//
//   var imgList = [
//
//     "assets/images/home/spider_man_1.jpg",
//
//     "assets/images/home/matrix.jpg",
//
//     "assets/images/home/thing.jpg",
//
//     "assets/images/home/predator.jpg"
//
//   ];
//
//
//   @override
//
//   Widget build(BuildContext context) {
//
//     // MediaQuery to get Device Width
//
//     double width = MediaQuery.of(context).size.width * 0.6;
//
//     return Scaffold(
//
//       appBar: AppBar(
//
//         // App Bar
//
//         title: Text(
//
//           "Movie App",
//
//           textAlign: TextAlign.center,
//
//           style: TextStyle(
//
//             fontFamily: "French Script MT Regular",
//
//             color: Colors.white,
//
//             fontSize: 25,
//
//           ),
//
//         ),
//
//         elevation: 0,
//
//         backgroundColor: Colors.pinkAccent,
//
//       ),
//
//       // Main List View With Builder
//
//       body: BackgroundHome(
//
//         decoration:const BoxDecoration(
//           image: DecorationImage(
//             image: AssetImage("assets/images/top_bg.png"),
//             fit: BoxFit.cover,
//           ),
//         ),
//         child: ListView.builder(
//
//           itemCount: imgList.length,
//
//           itemBuilder: (context, index) {
//
//             return GestureDetector(
//
//               onTap: () {
//
//                 // This Will Call When User Click On ListView Item
//
//                 showDialogFunc(context, imgList[index], titleList[index], descList[index]);
//
//               },
//
//               // Card Which Holds Layout Of ListView Item
//
//               child: Card(
//
//                 child: Row(
//
//                   children: <Widget>[
//
//                     Container(
//
//                       width: 100,
//
//                       height: 100,
//
//                       child: Image.asset(imgList[index]),
//
//                       child: Container(
//
//                         height: 200,
//
//                         width: 100,
//
//                         decoration: BoxDecoration(
//
//                           borderRadius: const BorderRadius.only(
//
//                             bottomLeft: Radius.circular(5),
//
//                             topLeft: Radius.circular(5),
//
//                           ),
//
//                           image: DecorationImage(
//
//                             fit: BoxFit.cover,
//
//                             image: Image.asset(imgList[index]),
//
//                           ),
//
//                         ),
//
//                       ),
//
//                     ),
//
//                     Padding(
//
//                       padding: const EdgeInsets.all(10.0),
//
//                       child: Column(
//
//                         crossAxisAlignment: CrossAxisAlignment.start,
//
//                         children: <Widget>[
//
//                           Text(
//
//                             titleList[index],
//
//                             style: TextStyle(
//
//                               fontSize: 25,
//
//                               color: Colors.black,
//
//                               fontWeight: FontWeight.bold,
//
//                             ),
//
//                           ),
//
//                           SizedBox(
//
//                             height: 10,
//
//                           ),
//
//                           Container(
//
//                             width: width,
//
//                             child: Text(
//
//                               descList[index],
//
//
//                               style: TextStyle(
//
//                                   fontSize: 7, color: Colors.black),
//
//                             ),
//
//                           ),
//
//                         ],
//
//                       ),
//
//                     )
//
//                   ],
//
//                 ),
//
//               ),
//
//             );
//
//           },
//
//         ),
//
//       ),
//
//     );
//
//   }
//
// }
//
//
// // This is a block of Model Dialog
//
// showDialogFunc(context, img, title, desc) {
//
//   return showDialog(
//
//     context: context,
//
//     builder: (context) {
//
//       return Center(
//
//         child: Material(
//
//           type: MaterialType.transparency,
//
//           child: Container(
//
//             decoration: BoxDecoration(
//
//               borderRadius: BorderRadius.circular(10),
//
//               color: Colors.white,
//
//             ),
//
//             padding: EdgeInsets.all(15),
//
//             height: 400,
//
//             width: MediaQuery.of(context).size.width * 0.7,
//
//             child: Column(
//
//               crossAxisAlignment: CrossAxisAlignment.center,
//
//               children: <Widget>[
//
//                 ClipRRect(
//
//                   borderRadius: BorderRadius.circular(5),
//
//                   child: Image.asset(
//
//                     img,
//
//                     width: 200,
//
//                     height: 200,
//
//                   ),
//
//                 ),
//
//                 SizedBox(
//
//                   height: 10,
//
//                 ),
//
//                 Text(
//
//                   title,textAlign:TextAlign.left ,
//
//                   style: TextStyle(
//
//                     fontSize: 25,
//
//                     color: Colors.black,
//
//                     fontWeight: FontWeight.bold,
//
//                   ),
//
//                 ),
//
//                 SizedBox(
//
//                   height: 10,
//
//                 ),
//
//                 Container(
//
//                   // width: 200,
//
//                   child: Align(
//
//                     alignment: Alignment.bottomLeft,
//
//                     child: Text(
//
//                       desc,
//
//
//                       style: TextStyle(fontSize: 10, color: Colors.black),
//
//                       textAlign: TextAlign.left,
//
//                     ),
//
//                   ),
//
//                 ),
//
//               ],
//
//             ),
//
//           ),
//
//         ),
//
//       );
//
//     },
//
//   );
//
// }