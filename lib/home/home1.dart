
import 'package:flutter/material.dart';
import 'package:movie/widgets/background_home.dart';

import 'movie.dart';
import 'movie_list.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          backgroundColor: Colors.pink,
          elevation: 0,
          centerTitle: true,
          title: const Text(
            "Movie App",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'French Script MT Regular',
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 35.0,
            ),
          ),
        ),
        body: BackgroundHome(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/top_bg.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: SingleChildScrollView(
            child: Column(children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height,
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: ListView.builder(
                  //physics: const NeverScrollableScrollPhysics(),
                  itemCount: movieList.length,
                  itemBuilder: (context, i) => MovieList(index: i),
                ),
              ),
            ]),
          ),
        ));
  }
}
