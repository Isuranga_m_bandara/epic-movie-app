import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class Data {
  int code;
  String name;
  String email;
  String userName;
  String password;

  Data({
    required this.code,
    required this.name,
    required this.email,
    required this.userName,
    required this.password,
  });

  //factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  Map<String, dynamic> toJson() => _$DataToJson(this);

  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(
      code: json['code'] as int,
      name: json['name'] as String,
      email: json['email'] as String,
      userName: json['username'] as String,
      password: json['avatar'] as String,
    );
  }

  Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
        'id': instance.code,
        'name': instance.name,
        'email': instance.email,
        'username': instance.userName,
        'password': instance.password,
      };

  Data copyWith({
    int? code,
    String? name,
    String? email,
    String? userName,
    String? password,
  }) {
    return Data(
      code: code ?? this.code,
      name: name ?? this.name,
      email: email ?? this.email,
      userName: userName ?? this.userName,
      password: password ?? this.password,
    );
  }
}
