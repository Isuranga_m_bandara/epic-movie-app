import 'dart:convert';

class User {
    User({
        required this.code,
        required this.movies,
    });

    final int code;
    final List<Movie> movies;

    factory User.fromJson(String str) => User.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory User.fromMap(Map<String, dynamic> json) => User(
        code: json["code"],
        movies: List<Movie>.from(json["movies"].map((x) => Movie.fromMap(x))),
    );

    Map<String, dynamic> toMap() => {
        "code": code,
        "movies": List<dynamic>.from(movies.map((x) => x.toMap())),
    };
}

class Movie {
    Movie({
        required this.id,
        required this.title,
        required this.description,
        required this.rating,
        required this.year,
        required this.category,
        required this.img,
    });

    final String id;
    final String title;
    final String description;
    final String rating;
    final String year;
    final String category;
    final String img;

    factory Movie.fromJson(String str) => Movie.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Movie.fromMap(Map<String, dynamic> json) => Movie(
        id: json["id"],
        title: json["title"],
        description: json["description"],
        rating: json["rating"],
        year: json["year"],
        category: json["category"],
        img: json["img"],
    );

    Map<String, dynamic> toMap() => {
        "id": id,
        "title": title,
        "description": description,
        "rating": rating,
        "year": year,
        "category": category,
        "img": img,
    };
}
