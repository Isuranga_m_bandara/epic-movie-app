class GetRegisterResponseEntity {
  GetRegisterResponseEntity({
    required this.code,
    required this.msg,
  });

  final int code;
  final String msg;
}
