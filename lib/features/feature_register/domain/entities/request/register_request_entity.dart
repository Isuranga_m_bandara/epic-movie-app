import 'package:movie/features/feature_register/data/models/request/register_request.dart';

class GetRegisterRequestEntity extends GetRegisterRequest {
  GetRegisterRequestEntity({
    required this.reqCode,
    required this.name,
    required this.email,
    required this.username,
    required this.password,
  }): super(reqCode: reqCode, name: name, email: email, username: username, password: password);

  final int reqCode;
  final String name;
  final String email;
  final String username;
  final String password;
}
