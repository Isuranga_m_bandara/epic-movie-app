import 'package:dartz/dartz.dart';
import 'package:movie/core/error/failures.dart';
import 'package:movie/features/feature_register/domain/entities/request/register_request_entity.dart';
import 'package:movie/features/feature_register/domain/entities/response/register_response_entity.dart';

abstract class RegisterRepository {
  Future<Either<Failure, GetRegisterResponseEntity>> getRegisterRequestRepository(
      GetRegisterRequestEntity getRegisterRequestEntity);
}