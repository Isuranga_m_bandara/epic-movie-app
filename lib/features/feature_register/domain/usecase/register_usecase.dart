import 'package:dartz/dartz.dart';
import 'package:movie/core/error/failures.dart';
import 'package:movie/core/usecase/usecase.dart';
import 'package:movie/features/feature_register/domain/entities/request/register_request_entity.dart';
import 'package:movie/features/feature_register/domain/entities/response/register_response_entity.dart';
import 'package:movie/features/feature_register/domain/repositories/register_repository.dart';

class GetRegisterUseCase
    extends Usecase<GetRegisterResponseEntity, GetRegisterRequestEntity> {
  final RegisterRepository repository;

  GetRegisterUseCase({required this.repository});
  @override
  Future<Either<Failure, GetRegisterResponseEntity>> call(
      GetRegisterRequestEntity params) async {
    return await repository.getRegisterRequestRepository(params);
  }
}
 