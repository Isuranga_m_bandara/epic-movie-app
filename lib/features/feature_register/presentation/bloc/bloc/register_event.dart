part of 'register_bloc.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();
}

class GetRegisterEvent extends RegisterEvent {
  final int reqCode;
  final String name;
  final String email;
  final String username;
  final String password;

  const GetRegisterEvent(
      {required this.reqCode, required this.name, required this.email, required this.username, required this.password});

  @override
  List<Object?> get props => throw UnimplementedError();
}
