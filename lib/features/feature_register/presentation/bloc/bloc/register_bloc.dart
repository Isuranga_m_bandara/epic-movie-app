import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:movie/core/error/failures.dart';
import 'package:movie/features/feature_register/data/models/request/register_request.dart';
import 'package:movie/features/feature_register/domain/entities/request/register_request_entity.dart';
import 'package:movie/features/feature_register/domain/entities/response/register_response_entity.dart';
import 'package:movie/features/feature_register/domain/usecase/register_usecase.dart';

part 'register_event.dart';
part 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final GetRegisterUseCase usecase;
  RegisterBloc({required this.usecase}) : super(RegisterInitialState());

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    if (event is GetRegisterEvent) {
      yield RegisterLoadingState();
      print(event.reqCode.toString() + "" + event.username);
      final result = await usecase(GetRegisterRequestEntity(
          reqCode: event.reqCode,
          name: event.name,
          email: event.email,
          username: event.username,
          password: event.password));
      yield result.fold(
          (l) => RegisterErrorState(error: _mapFailureToMessage(l)),
          (r) => RegisterSuccessState(registerResponseEntity: r));
    }
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return "Server Failure";
      default:
        return 'Unexpected Error';
    }
  }
}
