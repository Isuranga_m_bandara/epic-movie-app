part of 'register_bloc.dart';

abstract class RegisterState extends Equatable {
  const RegisterState();
}

class RegisterInitialState extends RegisterState {
  @override
  // TODO: implement props
  List<Object?> get props => throw UnimplementedError();
}

class RegisterLoadingState extends RegisterState {
  @override
  // TODO: implement props
  List<Object?> get props => throw UnimplementedError();
}

class RegisterSuccessState extends RegisterState {
  final GetRegisterResponseEntity registerResponseEntity;

  const RegisterSuccessState({required this.registerResponseEntity});
  @override
  List<Object?> get props => throw UnimplementedError();
}

class RegisterErrorState extends RegisterState {
  final String error;

  const RegisterErrorState({required this.error});
  @override
  // TODO: implement props
  List<Object?> get props => throw UnimplementedError();
}
