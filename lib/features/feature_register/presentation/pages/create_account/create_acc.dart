import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie/core/constants/api_list.dart';
import 'package:movie/core/injection/injection_container.dart';
import 'package:movie/features/feature_register/presentation/bloc/bloc/register_bloc.dart';

import 'package:movie/home/home1.dart';
import 'package:movie/features/feature_login/presentation/pages/login.dart';
import 'package:movie/widgets/background_image_create_acc.dart';

import 'Inputdeco.dart';

class FormPage extends StatefulWidget {
  @override
  _FormPageState createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {
  late String name, email, username;

  //TextController to read text entered in text field
  final nameEditingController = TextEditingController();
  final emailEditingController = TextEditingController();
  final usernameEditingController = TextEditingController();
  final passwordEditingController = TextEditingController();
  final confirmPasswordEditingController = TextEditingController();

  // final conname = new TextEditingController();
  // final conemail = new TextEditingController();
  // final conusername = new TextEditingController();
  // final conpass = new TextEditingController();
  // final conrepass = new TextEditingController();

  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  final _bloc = sl<RegisterBloc>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BackgroundImageacc(
        child: BlocProvider(
          create: (context) => _bloc,
          child: Center(
            child: SingleChildScrollView(
              child: BlocConsumer<RegisterBloc, RegisterState>(
                listener: (context, state) {
                  if (state is RegisterSuccessState) {
                    if (state.registerResponseEntity.code == 200) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          content: Text("Account Created Successfully!"),
                          backgroundColor: Colors.green,
                        ),
                      );
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => const LoginPage()));
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text(state.registerResponseEntity.msg),
                          backgroundColor: Colors.red,
                        ),
                      );
                    }
                  } else if (state is RegisterErrorState) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text(state.error.toString()),
                        backgroundColor: Colors.red,
                      ),
                    );
                  }
                },
                builder: (context, state) {
                  return Form(
                    key: _formkey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 80,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              bottom: 15, left: 10, right: 10),
                          child: SizedBox(
                            width: 200,
                            height: 60,
                            child: TextFormField(
                              keyboardType: TextInputType.text,
                              maxLength: 20,
                              decoration:
                                  buildInputDecoration(Icons.person, "Name"),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Please enter your name';
                                }
                                return null;
                              },
                              onSaved: (value) {
                                nameEditingController.text = value!;
                              },
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              bottom: 15, left: 10, right: 10),
                          child: SizedBox(
                            width: 200,
                            height: 60,
                            child: TextFormField(
                              keyboardType: TextInputType.text,
                              maxLength: 50,
                              decoration:
                                  buildInputDecoration(Icons.email, "Email"),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Please enter your email';
                                }
                                if (!RegExp(
                                        "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]")
                                    .hasMatch(value)) {
                                  return 'Please a valid Email';
                                }
                                return null;
                              },
                              onSaved: (value) {
                                emailEditingController.text = value!;
                              },
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            bottom: 10,
                            left: 30,
                            right: 30,
                          ),
                          child: SizedBox(
                            width: 200,
                            height: 60,
                            child: TextFormField(
                              keyboardType: TextInputType.text,
                              maxLength: 20,
                              style: new TextStyle(
                                  fontWeight: FontWeight.normal,
                                  color: Colors.black),
                              decoration: buildInputDecoration(
                                Icons.person,
                                "Username",
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Please enter your username';
                                }
                                return null;
                              },
                              onSaved: (value) {
                                usernameEditingController.text = value!;
                              },
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              bottom: 15, left: 30, right: 30),
                          child: SizedBox(
                            width: 200,
                            height: 60,
                            child: TextFormField(
                              controller: passwordEditingController,
                              keyboardType: TextInputType.text,
                              maxLength: 20,
                              decoration:
                                  buildInputDecoration(Icons.lock, "Password"),
                              onSaved: (value) {
                                passwordEditingController.text = value!;
                              },
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Please enter Password';
                                }
                                return null;
                              },
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              bottom: 0.1, left: 10, right: 10),
                          child: SizedBox(
                            width: 200,
                            height: 60,
                            child: TextFormField(
                              controller: confirmPasswordEditingController,
                              obscureText: true,
                              keyboardType: TextInputType.text,
                              maxLength: 20,
                              decoration: buildInputDecoration(
                                  Icons.lock, "Retype Password"),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Please re-enter password';
                                }
                                print(passwordEditingController.text);

                                print(confirmPasswordEditingController.text);

                                if (passwordEditingController.text !=
                                    confirmPasswordEditingController.text) {
                                  return "Password does not match";
                                }

                                return null;
                              },
                            ),
                          ),
                        ),
                        Column(children: [
                          Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image:
                                    AssetImage("assets/images/btn_1 (1).png"),
                              ),
                            ),
                            child: TextButton(
                              onPressed: () {
                                if (_formkey.currentState!.validate()) {
                                  BlocProvider.of<RegisterBloc>(context).add(
                                    GetRegisterEvent(
                                      reqCode: ApiCodes.regCode,
                                      name: nameEditingController.text,
                                      email: emailEditingController.text,
                                      username: usernameEditingController.text,
                                      password: passwordEditingController.text,
                                    ),
                                  );
                                }
                              },
                              child: Text(
                                'Create Account',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20.0,
                                ),
                              ),
                            ),
                          ),
                          Container(
                              child: state is RegisterLoadingState
                                ? const CircularProgressIndicator()
                                    : null,
                          ),
                        ]),
                        SizedBox(
                          height: 40,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 0.0),
                          child: SizedBox(
                            height: 50,
                            child: Container(
                              decoration: BoxDecoration(
                                border:
                                    Border.all(color: Colors.white, width: 2),
                                shape: BoxShape.rectangle,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(25.0)),
                              ),
                              child: SizedBox(
                                width: 200,
                                height: 200,
                                child: TextButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => LoginPage()));
                                  },
                                  child: Text(
                                    'Cancel',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20.0,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}
