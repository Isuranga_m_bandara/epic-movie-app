import 'package:dio/dio.dart';
import 'package:movie/core/constants/api_list.dart';
import 'package:movie/features/feature_register/data/models/request/register_request.dart';
import 'package:movie/features/feature_register/data/models/response/register_response.dart';

abstract class RemoteRegisterDataSource {
  Future<GetRegisterResponse> registerRequest(GetRegisterRequest getRegisterRequest);
}

class RemoteRegisterDataSourceImpl implements RemoteRegisterDataSource {
  final Dio _dio = Dio();

  @override
  Future<GetRegisterResponse> registerRequest(GetRegisterRequest getRegisterRequest) async {
    try {
      final response = await _dio.post(baseUrl, data: getRegisterRequest.toJson());
      return GetRegisterResponse.fromJson(response.data);
    } on Exception catch (e) {
      throw e;
    }
  }
}