import 'dart:convert';
import 'package:movie/features/feature_register/domain/entities/response/register_response_entity.dart';

GetRegisterResponse getRegisterResponseFromJson(String str) =>
    GetRegisterResponse.fromJson(json.decode(str));

String getRegisterResponseToJson(GetRegisterResponse data) =>
    json.encode(data.toJson());

class GetRegisterResponse extends GetRegisterResponseEntity {
  GetRegisterResponse({
    required this.code,
    required this.msg,
  }) : super(code: code, msg: msg);

  final int code;
  final String msg;

  factory GetRegisterResponse.fromJson(Map<String, dynamic> json) =>
      GetRegisterResponse(
        code: json["code"],
        msg: json["msg"],
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "msg": msg,
      };
}
