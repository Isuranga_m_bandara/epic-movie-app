import 'package:dartz/dartz.dart';
import 'package:movie/core/error/exception.dart';
import 'package:movie/core/error/failures.dart';
import 'package:movie/core/network/network_info.dart';
import 'package:movie/features/feature_register/data/data_source/remote_data_source/remote_register_data_source.dart';
import 'package:movie/features/feature_register/domain/entities/request/register_request_entity.dart';
import 'package:movie/features/feature_register/domain/entities/response/register_response_entity.dart';
import 'package:movie/features/feature_register/domain/repositories/register_repository.dart';

class RegisterRepositoryImpl implements RegisterRepository {
  final NetworkInfo networkInfo;
  final RemoteRegisterDataSource remoteData;

  RegisterRepositoryImpl({required this.networkInfo, required this.remoteData});

  @override
  Future<Either<Failure, GetRegisterResponseEntity>> getRegisterRequestRepository(
      GetRegisterRequestEntity getRegisterRequestEntity) async {
    if (await networkInfo.isConnected) {
      try {
        return Right(await remoteData.registerRequest(getRegisterRequestEntity));
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }
}