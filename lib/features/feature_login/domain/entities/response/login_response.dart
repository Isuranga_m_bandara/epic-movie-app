class GetLoginResponseEntity{
  GetLoginResponseEntity({
    required this.code,
    required this.msg,
    this.token,
  });

  int code;
  String msg;
  String? token;
}
