import 'package:movie/features/feature_login/data/model/request/login_request.dart';

class GetLoginRequestEntity extends GetLoginRequest{
  GetLoginRequestEntity({
    required this.reqCode,
    required this.username,
    required this.password,
  }): super(reqCode: reqCode,username: username, password: password);

  int reqCode;
  String username;
  String password;
}