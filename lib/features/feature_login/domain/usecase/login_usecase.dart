import 'package:movie/core/error/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:movie/core/usecase/usecase.dart';
import 'package:movie/features/feature_login/domain/entities/request/login_request.dart';
import 'package:movie/features/feature_login/domain/entities/response/login_response.dart';
import 'package:movie/features/feature_login/domain/repositories/login_repository.dart';

class GetLoginUsecase
    extends Usecase<GetLoginResponseEntity, GetLoginRequestEntity> {
  final LoginRepository repository;

  GetLoginUsecase({required this.repository});
  @override
  Future<Either<Failure, GetLoginResponseEntity>> call(
      GetLoginRequestEntity params) async {
    return await repository.getLoginRequestRepository(params);
  }
}
