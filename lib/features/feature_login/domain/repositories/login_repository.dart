import 'package:dartz/dartz.dart';
import 'package:movie/core/error/failures.dart';
import 'package:movie/features/feature_login/domain/entities/request/login_request.dart';
import 'package:movie/features/feature_login/domain/entities/response/login_response.dart';

abstract class LoginRepository {
  Future<Either<Failure, GetLoginResponseEntity>> getLoginRequestRepository(
      GetLoginRequestEntity getLoginRequestEntity);
}
