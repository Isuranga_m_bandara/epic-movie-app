import 'package:movie/core/constants/api_list.dart';
import 'package:movie/features/feature_login/data/model/request/login_request.dart';
import 'package:movie/features/feature_login/data/model/response/login_response.dart';

import 'package:dio/dio.dart';

abstract class RemoteLoginDataSource {
  Future<GetLoginResponse> loginRequest(GetLoginRequest getLoginRequest);
}

class RemoteLoginDataSourceImpl implements RemoteLoginDataSource{

  final Dio _dio = Dio();

  @override
  Future<GetLoginResponse> loginRequest(GetLoginRequest getLoginRequest) async {
    try{
      final response = await _dio.post(baseUrl, data: getLoginRequest.toJson());
      print(response.data);
      return GetLoginResponse.fromJson(response.data);
    } on Exception catch (e) {
      throw e;
    }
  

  }
  
}
