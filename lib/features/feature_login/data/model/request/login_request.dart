import 'dart:convert';

GetLoginRequest getLoginRequestFromJson(String str) => GetLoginRequest.fromJson(json.decode(str));

String getLoginRequestToJson(GetLoginRequest data) => json.encode(data.toJson());

class GetLoginRequest {
  GetLoginRequest({
    required this.reqCode,
    required this.username,
    required this.password,
  });

  int reqCode;
  String username;
  String password;

  factory GetLoginRequest.fromJson(Map<String, dynamic> json) => GetLoginRequest(
    reqCode: json["req_code"],
    username: json["username"],
    password: json["password"],
  );

  Map<String, dynamic> toJson() => {
    "req_code": reqCode,
    "username": username,
    "password": password,
  };
}