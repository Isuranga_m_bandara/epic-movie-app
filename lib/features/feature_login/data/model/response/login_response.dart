import 'dart:convert';
import 'package:movie/features/feature_login/domain/entities/response/login_response.dart';

GetLoginResponse getLoginResponseFromJson(String str) => GetLoginResponse.fromJson(json.decode(str));

String getLoginResponseToJson(GetLoginResponse data) => json.encode(data.toJson());

class GetLoginResponse extends GetLoginResponseEntity {
  GetLoginResponse({
    required this.code,
    required this.msg,
    this.token,
  }) : super(code: code, msg:msg, token: token);

  int code;
  String msg;
  String? token;

  factory GetLoginResponse.fromJson(Map<String, dynamic> json) => GetLoginResponse(
    code: json["code"],
    msg: json["msg"],
    token: json["token"],
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "msg": msg,
    "token": token,
  };
}
