import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie/core/injection/injection_container.dart';
import 'package:movie/features/feature_login/presentation/bloc/login/bloc/login_bloc.dart';

class LoginSample extends StatefulWidget {
  const LoginSample({Key? key}) : super(key: key);

  @override
  _LoginSampleState createState() => _LoginSampleState();
}

class _LoginSampleState extends State<LoginSample> {
  final bloc = sl<LoginBloc>();

  void _dispatchInit(context) {
    BlocProvider.of<LoginBloc>(context).add(const GetLoginEvent(
        reqCode: 101, username: "arvinjayanake", password: "YWJjMTIz#e"));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BlocProvider(
        create: (_) => bloc,
        child: BlocBuilder<LoginBloc, LoginState>(
          builder: (context, state) {
            if (state is LoginInitialState) {
              _dispatchInit(context);
              return const CircularProgressIndicator();
            } else if (state is LoginLoadingState) {
              return const CircularProgressIndicator();
            } else if (state is LoginSuccessState) {
              return Text(state.loginResponseEntity.msg.toString());
            } else if (state is LoginErrorState) {
              return ErrorWidget(state.error.toString());
            }
            return ErrorWidget("exception");
          },
        ),
      ),
    );
  }
}