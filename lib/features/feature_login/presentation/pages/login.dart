import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie/core/constants/api_list.dart';
import 'package:movie/core/injection/injection_container.dart';

import 'package:movie/features/feature_login/presentation/bloc/login/bloc/login_bloc.dart';
import 'package:movie/features/feature_register/presentation/pages/create_account/Inputdeco.dart';
import 'package:movie/features/feature_register/presentation/pages/create_account/create_acc.dart';

import 'package:movie/home/home1.dart';
import 'package:movie/palatte.dart';
import 'package:movie/widgets/background_image.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final namecon = new TextEditingController();
  final passcon = new TextEditingController();
  final bloc = sl<LoginBloc>();
  final int code = 100;
  bool islogin = false;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        BackgroundImage(),
        Scaffold(
          backgroundColor: Colors.transparent,
          body: SafeArea(
            child: Column(
              children: [
                SizedBox(
                  height: 100,
                ),
                Container(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 40,
                    vertical: 10,
                  ),
                  child: Column(
                    children: [
                      Column(
                        children: [
                          TextFormField(
                            controller: namecon,
                            keyboardType: TextInputType.text,
                            maxLength: 20,
                            style: new TextStyle(
                                fontWeight: FontWeight.normal,
                                color: Colors.black),
                            decoration: buildInputDecoration(
                              Icons.person,
                              "Username",
                            ),
                            onSaved: (value) {
                              namecon.text = value!;
                            },
                            textInputAction: TextInputAction.next,
                          ),
                          TextFormField(
                            autofocus: false,
                            controller: passcon,
                            obscureText: true,
                            onSaved: (value) {
                              passcon.text = value!;
                            },
                            textInputAction: TextInputAction.done,
                            keyboardType: TextInputType.text,
                            maxLength: 20,
                            decoration:
                                buildInputDecoration(Icons.lock, "Password"),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Container(
                            width: double.infinity,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image:
                                    AssetImage("assets/images/btn_1 (1).png"),
                              ),
                            ),
                            child: TextButton(
                              onPressed: () {
                                if (namecon.text != "" && passcon.text != "") {
                                  setState(() {
                                    islogin = true;
                                  });
                                  //  _dispatchInit(newcontext);
                                  // BlocProvider.of<LoginBloc>(context)..add(GetLoginEvent(
                                  //   reqCode: code,
                                  //       username: namecon.text,
                                  //        password: passcon.text
                                  // ));
                                  // class LoginBloc extends Bloc<LoginEvent, LoginState> {
                                  // final GetCompanyUseCase useCase;

                                  // LoginBloc({required this.UseCase})
                                  //  : super(LoginInitialState());}
                                  // BlocProvider.of<LoginBloc>(context)
                                  //   ..add(GetLoginEvent(
                                  //       reqCode: code,
                                  //       username: namecon.text,
                                  //       password: passcon.text));
                                } else {
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          title: Text("Aleart"),
                                          content: Text(
                                              "Please enter username and password to login"),
                                          actions: [
                                            TextButton(
                                                onPressed: () =>
                                                    Navigator.of(context).pop(),
                                                child: Text('OK'))
                                          ],
                                        );
                                      });
                                }
                              },
                              child: islogin == false
                                  ? Text(
                                      'Login',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20.0,
                                      ),
                                    )
                                  : buildBloc(context),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          SizedBox(
                            height: 90,
                          ),
                          Container(
                            width: double.infinity,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image:
                                    AssetImage("assets/images/btn_1 (1).png"),
                              ),
                            ),
                            child: TextButton(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => FormPage()));
                              },
                              child: Text(
                                'Create Account',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20.0,
                                ),
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  BlocProvider<LoginBloc> buildBloc(BuildContext appContext) {
    return BlocProvider(
      create: (_) => bloc,
      child: BlocBuilder<LoginBloc, LoginState>(
        builder: (appContext, state) {
          if (state is LoginInitialState) {
            _dispatchInit(context);
            // BlocProvider.of<LoginBloc>(context)
            //   ..add(GetLoginEvent(
            //       reqCode: code,
            //       username: namecon.text,
            //       password: passcon.text));
            return const CircularProgressIndicator();
          } else if (state is LoginLoadingState) {
            return const CircularProgressIndicator();
          } else if (state is LoginSuccessState) {
            if (state.loginResponseEntity.code == 200) {
              AppConstants.loginToken =
                  state.loginResponseEntity.token.toString();
              Navigator.of(appContext).pushReplacement(
                  MaterialPageRoute(builder: (appContext) => const Home()));
            } else {
              return Text(state.loginResponseEntity.msg);
            }
          } else if (state is LoginErrorState) {
            return ErrorWidget(state.error.toString());
          }
          return ErrorWidget("exception");
        },
      ),
    );
  }
  //newtest123
  //NewTest@123

  void _dispatchInit(context) {
    BlocProvider.of<LoginBloc>(context)
      ..add(GetLoginEvent(
          reqCode: code, username: namecon.text, password: passcon.text));
  }
}

class TextInput extends StatelessWidget {
  const TextInput({
    Key? key,
    required this.hint,
    required this.inputAction,
  }) : super(key: key);

  final String hint;

  final TextInputAction inputAction;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12.0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(16),
        ),
        child: TextField(
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.symmetric(
              vertical: 5,
              horizontal: 10,
            ),
            border: InputBorder.none,
            hintText: hint,
            hintStyle: kBodyText,
          ),
          style: kBodyText,
          maxLength: 20,
          textInputAction: inputAction,
        ),
      ),
    );
  }
}

class PasswordInput extends StatelessWidget {
  const PasswordInput({
    Key? key,
    required this.hint,
    required this.inputAction,
  }) : super(key: key);

  final String hint;

  final TextInputAction inputAction;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12.0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(16),
        ),
        child: TextField(
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.symmetric(
              vertical: 2,
              horizontal: 5,
            ),
            border: InputBorder.none,
            hintText: hint,
            hintStyle: kBodyText,
          ),
          style: kBodyText,
          obscureText: true,
          textInputAction: inputAction,
          maxLength: 20,
        ),
      ),
    );
  }
}
