part of 'login_bloc.dart';

abstract class LoginState extends Equatable {
  const LoginState();
}

class LoginInitialState extends LoginState {
  @override
  // TODO: implement props
  List<Object?> get props => throw UnimplementedError();
}

class LoginLoadingState extends LoginState {
  @override
  // TODO: implement props
  List<Object?> get props => throw UnimplementedError();
}

class LoginSuccessState extends LoginState {
  final GetLoginResponseEntity loginResponseEntity;

  const LoginSuccessState({required this.loginResponseEntity});
  @override
  List<Object?> get props => throw UnimplementedError();
}

class LoginErrorState extends LoginState {
  final String error;
  const LoginErrorState({required this.error});
  @override
  List<Object?> get props => throw UnimplementedError();
}
