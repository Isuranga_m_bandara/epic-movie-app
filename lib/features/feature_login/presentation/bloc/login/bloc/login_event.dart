part of 'login_bloc.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();
}

class GetLoginEvent extends LoginEvent {
  final int reqCode;
  final String username;
  final String password;

  const GetLoginEvent(
      {required this.reqCode, required this.password, required this.username});
  @override
  List<Object?> get props => throw UnimplementedError();
}
