import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:movie/core/error/failures.dart';
import 'package:movie/features/feature_login/domain/entities/request/login_request.dart';
import 'package:movie/features/feature_login/domain/entities/response/login_response.dart';
import 'package:movie/features/feature_login/domain/usecase/login_usecase.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final GetLoginUsecase usecase;
  LoginBloc({required this.usecase}) : super(LoginInitialState());

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is GetLoginEvent) {
      yield LoginLoadingState();
      final result = await usecase(GetLoginRequestEntity(
          reqCode: event.reqCode,
          username: event.username,
          password: event.password));
      yield result.fold((l) => LoginErrorState(error: _mapFailureToMessage(l)),
          (r) => LoginSuccessState(loginResponseEntity: r));
    }
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return "Server Failure";
      default:
        return 'Unexpected Error';
    }
  }
}
