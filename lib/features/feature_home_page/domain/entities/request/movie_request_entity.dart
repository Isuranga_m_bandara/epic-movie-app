import 'package:movie/features/feature_home_page/data/models/request/movie_request.dart';

class GetMovieRequestEntity extends GetMovieRequest {
  GetMovieRequestEntity({
    required this.reqCode,
    required this.token,
  }): super(reqCode: reqCode, token: token);

  final int reqCode;
  final String token;
}