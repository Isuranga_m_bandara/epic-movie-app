import 'package:dartz/dartz.dart';
import 'package:movie/core/error/failures.dart';
import 'package:movie/features/feature_home_page/domain/entities/request/movie_request_entity.dart';
import 'package:movie/features/feature_home_page/domain/entities/response/movie_response_entity.dart';

abstract class MovieRepository {
  Future<Either<Failure, List<GetMovieResponseEntity>>>
      getMovieRequestRepository(
          GetMovieRequestEntity getMovieRequestEntity);
}
