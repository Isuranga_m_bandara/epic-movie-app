import 'package:dartz/dartz.dart';
import 'package:movie/core/error/failures.dart';
import 'package:movie/core/usecase/usecase.dart';

import 'package:movie/features/feature_home_page/domain/entities/request/movie_request_entity.dart';
import 'package:movie/features/feature_home_page/domain/entities/response/movie_response_entity.dart';
import 'package:movie/features/feature_home_page/domain/repositories/movie_repository.dart';

class GetMovieUseCase
    extends Usecase<List<GetMovieResponseEntity>, GetMovieRequestEntity> {
  final MovieRepository repository;

  GetMovieUseCase({required this.repository});
  @override
  Future<Either<Failure, List<GetMovieResponseEntity>>> call(
      GetMovieRequestEntity params) async {
    return await repository.getMovieRequestRepository(params);
  }
}
