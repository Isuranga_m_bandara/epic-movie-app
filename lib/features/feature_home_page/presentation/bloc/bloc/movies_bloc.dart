// import 'package:bloc/bloc.dart';
// import 'package:equatable/equatable.dart';
// import 'package:movie/core/error/failures.dart';
// import 'package:movie/features/feature_home_page/domain/entities/request/movie_request_entity.dart';
// import 'package:movie/features/feature_home_page/domain/entities/response/movie_response_entity.dart';
// import 'package:movie/features/feature_home_page/domain/usecase/movie_usecase.dart';


// part 'movies_event.dart';
// part 'movies_state.dart';

// class MovieBloc extends Bloc<MoviesEvent, MoviesState> {
//   final GetMovieUseCase usecase;
//   MovieBloc({required this.usecase}) : super(MoviesInitialState());

//   @override
//   Stream<MoviesState> mapEventToState(MoviesEvent event) async* {
//     if (event is GetMoviesEvent) {
//       yield MoviesLoadingState();
//       final result = await usecase(GetMovieRequestEntity(
//           reqCode: event.reqCode,
//           token: event.token,),);
//       yield result.fold((l) => MoviesErrorState(error: _mapFailureToMessage(l)),
//           (r) => MoviesSuccessState(movieResponseEntity: r));
//     }
//   }

//   String _mapFailureToMessage(Failure failure) {
//     switch (failure.runtimeType) {
//       case ServerFailure:
//         return "Server Failure";
//       default:
//         return 'Unexpected Error';
//     }
//   }
// }
