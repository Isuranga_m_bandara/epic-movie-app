import 'dart:convert';

import 'package:movie/features/feature_home_page/domain/entities/response/movie_response_entity.dart';



GetMovieResponse getMovieResponseFromJson(String str) =>
    GetMovieResponse.fromJson(json.decode(str));

String getMovieResponseToJson(GetMovieResponse data) =>
    json.encode(data.toJson());

class GetMovieResponse extends GetMovieResponseEntity {
  GetMovieResponse({
    required this.code,
    required this.movies,
  }) : super(code: code, movies: movies);

  final int code;
  final List<Movie> movies;

  factory GetMovieResponse.fromJson(Map<String, dynamic> json) =>
      GetMovieResponse(
        code: json["code"],
        movies: List<Movie>.from(json["movies"].map((x) => Movie.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "movies": List<dynamic>.from(movies.map((x) => x.toJson())),
      };
}

class Movie extends MovieEntity {
  Movie({
    required this.id,
    required this.title,
    required this.description,
    required this.rating,
    required this.year,
    required this.category,
    required this.img,
  }) : super(
          id: id,
          title: title,
          description: description,
          rating: rating,
          year: year,
          category: category,
          img: img,
        );

  final String id;
  final String title;
  final String description;
  final String rating;
  final String year;
  final String category;
  final String img;

  factory Movie.fromJson(Map<String, dynamic> json) => Movie(
        id: json["id"],
        title: json["title"],
        description: json["description"],
        rating: json["rating"],
        year: json["year"],
        category: json["category"],
        img: json["img"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "description": description,
        "rating": rating,
        "year": year,
        "category": category,
        "img": img,
      };
}
