import 'dart:convert';

GetMovieRequest getMovieRequestFromJson(String str) =>
    GetMovieRequest.fromJson(json.decode(str));

String getMovieRequestToJson(GetMovieRequest data) =>
    json.encode(data.toJson());

class GetMovieRequest {
  GetMovieRequest({
    required this.reqCode,
    required this.token,
  });

  final int reqCode;
  final String token;

  factory GetMovieRequest.fromJson(Map<String, dynamic> json) =>
      GetMovieRequest(
        reqCode: json["req_code"],
        token: json["token"],
      );

  Map<String, dynamic> toJson() => {
        "req_code": reqCode,
        "token": token,
      };
}
