import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:movie/core/constants/api_list.dart';
import 'package:movie/core/error/exception.dart';
import 'package:movie/features/feature_home_page/data/models/request/movie_request.dart';
import 'package:movie/features/feature_home_page/data/models/response/movie_response.dart';

abstract class RemoteMovieDataSource {
  Future<List<GetMovieResponse>> movieRequest(GetMovieRequest getMovieRequest);
}

class RemoteMovieDataSourceImpl implements RemoteMovieDataSource {
  final Dio _dio = Dio();

  @override
  Future<List<GetMovieResponse>> movieRequest(
      GetMovieRequest getMovieRequest) async {
    final response = await _dio.post(baseUrl, data: getMovieRequest.toJson());

    if (response.statusCode == 200) {
      var data = json.decode(response.data);
      return data
          .map<GetMovieResponse>((json) => GetMovieResponse.fromJson(json))
          .toList();
    } else {
      throw ServerException();
    }
  }
}