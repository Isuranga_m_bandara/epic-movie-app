const int NetWORK_CON_TIMEOUT = 5000;
const String baseUrl = 'https://arvinapps.com/movies/api.php';

class AppConstants {
  static String loginToken = "";
  static int loginCode = 101;

  static bool loginIsSelect = false;

  static String loginUsername = "";
  static String loginPassword = "";
}

class ApiCodes {
  static int regCode = 100;
  static int loginCode = 101;
  static int movieCode = 102;
}
