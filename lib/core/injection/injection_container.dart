import 'package:connectivity/connectivity.dart';
import 'package:get_it/get_it.dart';
import 'package:movie/core/network/network_info.dart';
import 'package:movie/features/feature_login/data/data_sources/remote_data_source/remote_data_source.dart';
import 'package:movie/features/feature_login/data/repositories/login_repository.dart';
import 'package:movie/features/feature_login/domain/repositories/login_repository.dart';
import 'package:movie/features/feature_login/domain/usecase/login_usecase.dart';
import 'package:movie/features/feature_login/presentation/bloc/login/bloc/login_bloc.dart';
import 'package:movie/features/feature_register/data/data_source/remote_data_source/remote_register_data_source.dart';
import 'package:movie/features/feature_register/data/repositories/register_repository.dart';
import 'package:movie/features/feature_register/domain/repositories/register_repository.dart';
import 'package:movie/features/feature_register/domain/usecase/register_usecase.dart';
import 'package:movie/features/feature_register/presentation/bloc/bloc/register_bloc.dart';

final sl = GetIt.instance;

Future<void> init() async {
  //Call Login BLoC
  sl.registerLazySingleton(() => LoginBloc(usecase: sl()));
  sl.registerLazySingleton(() => RegisterBloc(usecase: sl()));

  //Data Source
  sl.registerLazySingleton<RemoteLoginDataSource>(() => RemoteLoginDataSourceImpl());
  sl.registerLazySingleton<RemoteRegisterDataSource>(() => RemoteRegisterDataSourceImpl());

  //Repositories
  sl.registerLazySingleton<LoginRepository>(
      () => LoginRepositoryImpl(networkInfo: sl(), remoteData: sl()));
  sl.registerLazySingleton<RegisterRepository>(
      () => RegisterRepositoryImpl(networkInfo: sl(), remoteData: sl()));

  //Usecases
  sl.registerLazySingleton(() => GetLoginUsecase(repository: sl()));
  sl.registerLazySingleton(() => GetRegisterUseCase(repository: sl()));

  //Netowork
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl());
  sl.registerLazySingleton(() => Connectivity());
}
