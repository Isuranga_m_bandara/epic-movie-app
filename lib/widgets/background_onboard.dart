import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';

class BackgroundImageOnboard extends StatefulWidget {
  const BackgroundImageOnboard ({Key? key, required IntroSlider child}) : super(key: key);

  @override
  _BackgroundImageOnboardState createState() => _BackgroundImageOnboardState();
}

class _BackgroundImageOnboardState extends State<BackgroundImageOnboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children:<Widget> [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(image:
              AssetImage("assets/images/bg.png"),
                  fit: BoxFit.cover),

            ),
          ),
          Container(margin: EdgeInsets.only(top: 90,left: 10,right: 10,bottom: 150),
            decoration: BoxDecoration(
              color: const Color(0xFF0000000).withOpacity(0.06),
              border: new Border.all(color: Colors.transparent),
              borderRadius: new BorderRadius.only(
                  topLeft: const Radius.circular(20.0),
                  topRight: const Radius.circular(20.0),
                  bottomLeft: const Radius.circular(20.0),
                  bottomRight: const Radius.circular(20.0)
              ),
            ),

          ),




        ],


      ),
    );



  }
}