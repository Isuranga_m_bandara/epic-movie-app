import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BackgroundImage extends StatelessWidget {
  const BackgroundImage ({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children:<Widget> [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(image:
              AssetImage("assets/images/bg.png"),
                  fit: BoxFit.cover),

            ),
          ),
          Container(margin: EdgeInsets.only(top: 90,left: 10,right: 10,bottom: 150),
            decoration: BoxDecoration(
              color: const Color(0xFF0000000).withOpacity(0.06),
              border: new Border.all(color: Colors.transparent),
              borderRadius: new BorderRadius.only(
                  topLeft: const Radius.circular(20.0),
                  topRight: const Radius.circular(20.0),
                  bottomLeft: const Radius.circular(20.0),
                  bottomRight: const Radius.circular(20.0)
              ),
            ),

          ),
          Container(
            padding:EdgeInsets.fromLTRB(100, 10, 0, 120),
            child: Column(

              children:<Widget> [

                Text(
                  "Movie App",
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: "French Script MT Regular",
                    fontSize: 30.0,
                  ),
                ),
              ],
            ),
          ),

          Container(
            padding:EdgeInsets.fromLTRB(120, 90, 0, 100),
            child: Column(

              children:<Widget> [

                Text(
                  "Login",
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: "French Script MT Regular",
                    fontSize: 30.0,
                  ),
                ),
              ],
            ),
          ),

        ],


      ),
    );



  }
}