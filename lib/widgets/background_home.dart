import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BackgroundHome extends StatefulWidget {
  const BackgroundHome ({Key? key, @required this.child, required BoxDecoration decoration}) : super(key: key);

  final child;

  @override
  _BackgroundImageaccState createState() => _BackgroundImageaccState();
}

class _BackgroundImageaccState extends State<BackgroundHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children:<Widget> [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(image:
              AssetImage("assets/images/home/top_bg.png"),
                  fit: BoxFit.cover),

            ),
          ),

          Container(
            padding:EdgeInsets.fromLTRB(100, 10, 0, 120),
            child: Column(

              children:<Widget> [

                Text(
                  "Movie App",
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: "French Script MT Regular",
                    fontSize: 30.0,
                  ),
                ),
              ],
            ),
          ),


          widget.child,
        ],




      ),
    );



  }
}